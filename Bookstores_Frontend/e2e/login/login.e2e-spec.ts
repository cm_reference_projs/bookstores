import { LoginPage } from './login.po';

describe('Login page', () => {
  let page: LoginPage;

  beforeEach(() => {
    page = new LoginPage();
  });

  it('should login testUser', () => {
    page.navigateTo();
    page.setLogin('testUser');
    page.setPassword('123456');
    page.login();
    expect(page.isLoginFormPresent()).toBe(false);
  });
});
