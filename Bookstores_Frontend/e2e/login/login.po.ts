import { browser, by, element } from 'protractor';

export class LoginPage {

  navigateTo() {
    return browser.get('/login');
  }

  setLogin(userName: string) {
    element(by.id('userName')).sendKeys(userName);
  }

  setPassword(password: string) {
    element(by.id('password')).sendKeys(password);
  }

  login() {
    element(by.id('loginBtn')).click();
  }

  isLoginFormPresent() {
    return element(by.id('loginForm')).isPresent();
  }

}
