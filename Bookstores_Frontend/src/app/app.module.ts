import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BooksListComponent } from './books/books-list/books-list.component';
import {HttpClientModule} from '@angular/common/http';
import {BookService} from './books/book.service';
import { MessageService } from './message/message.service';
import { MessageComponent } from './message/message/message.component';
import { BookDetailsComponent } from './books/book-details/book-details.component';
import {RouterModule, Routes} from '@angular/router';
import { AddBookComponent } from './books/add-book/add-book.component';
import {FormsModule} from '@angular/forms';
import { LoginComponent } from './security/login/login.component';
import {UserService} from './security/user/user.service';
import {AuthenticationService} from './security/authentication/authentication-service.service';
import {AuthorizationGuard} from './security/authorization/authorization.guard';
import { NavComponent } from './nav/nav/nav.component';

const routes: Routes = [
  { path: '', canActivate: [AuthorizationGuard], canActivateChild: [AuthorizationGuard], children: [
      { path: '', redirectTo: '/books', pathMatch: 'full'},
      { path: 'books', component: BooksListComponent },
      { path: 'book', children: [
        { path: 'add', component: AddBookComponent },
        { path: ':id', component: BookDetailsComponent }
      ]},
    ]},
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  declarations: [
    AppComponent,
    BooksListComponent,
    MessageComponent,
    BookDetailsComponent,
    AddBookComponent,
    LoginComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    BookService,
    UserService,
    MessageService,
    AuthenticationService,
    AuthorizationGuard
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
