import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
// import {BooksListComponent} from "./books/books-list/books-list.component";
// import {BookDetailsComponent} from "./books/book-details/book-details.component";

// const routes: Routes = [
//   { path: '', redirectTo: '/books', pathMatch: 'full' },
//   { path: 'books', component: BooksListComponent },
//   { path: 'book/:id', component: BookDetailsComponent }
// ]

@NgModule({
  imports: [
    // RouterModule.forRoot(routes),
    CommonModule
  ],
  // export: [
  //   RouterModule
  // ],
  declarations: []
})

export class AppRoutingModule { }
