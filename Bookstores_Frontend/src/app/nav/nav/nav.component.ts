import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../../security/authentication/authentication-service.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit() {
  }

  logout() {
    this.authenticationService.logout();
  }

  isLoggedIn(): boolean {
    return this.authenticationService.isUserLogged();
  }
}
