import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild} from "@angular/router";
import { Observable } from 'rxjs/Observable';
import {UserService} from '../user/user.service';

@Injectable()
export class AuthorizationGuard implements CanActivate, CanActivateChild {

  constructor(private userService: UserService,
              private router: Router) {
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.evaluateAccessibility();
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.evaluateAccessibility();
  }

  evaluateAccessibility(): Observable<boolean> | Promise<boolean> | boolean {
    const currentUser = this.userService.getCurrentUser();

    if (!currentUser || !currentUser.isAuthenticated ) {
      this.router.navigateByUrl('login');
      return false;
    }

    if (!currentUser.isAuthorized) {
      return false;
    }

    return true;
  }
}
