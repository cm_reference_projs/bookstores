import { Injectable } from '@angular/core';
import {User} from '../user';

@Injectable()
export class UserService {

  constructor() { }

  setCurrentUser(user: User) {
    const userJson = JSON.stringify(user);
    localStorage.setItem('user', userJson);
  }

  getCurrentUser(): User {
    const userJson = localStorage.getItem('user');

    if (userJson) {
      return JSON.parse(userJson) as User;
    }

    return null;
  }

  removeUser() {
    localStorage.removeItem('user');
  }
}
