import { Injectable } from '@angular/core';
import {Credentials} from '../credentials';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../user';
import {environment} from '../../../environments/environment';
import {MessageService} from '../../message/message.service';
import {map} from 'rxjs/operators';
import {UserService} from '../user/user.service';
import {Router} from '@angular/router';

@Injectable()
export class AuthenticationService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private userService: UserService) {
  }

  login(credentials: Credentials): Observable<User> {
    return this.http.post<any>(environment.bookstoreApi + 'authentication', credentials)
      .pipe(map(response => {
        // Aplikacja zaklada, ze jezeli mam konto, to mam dostep wszedzie
        return new User(response.UserName, response.IsAutheticated, response.IsAutheticated);
      }));
  }

  logout() {
    this.userService.removeUser();
    this.router.navigateByUrl('');
  }

  isUserLogged(): boolean {
    const user = this.userService.getCurrentUser();
    const result = user !== null;
    return result;
  }

}
