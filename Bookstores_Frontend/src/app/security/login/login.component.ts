import { Component, OnInit } from '@angular/core';
import {Credentials} from "../credentials";
import {AuthenticationService} from "../authentication/authentication-service.service";
import {UserService} from "../user/user.service";
import {Router} from "@angular/router";
import {MessageService} from "../../message/message.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  userCredentials: Credentials = new Credentials();

  constructor(private authenticationService: AuthenticationService,
              private userService: UserService,
              private messageService: MessageService,
              private router: Router) {
  }

  ngOnInit() {
  }

  login() {
    this.authenticationService.login(this.userCredentials)
      .subscribe(
        data => {
          if (data.isAuthenticated){
            this.userService.setCurrentUser(data);
            this.router.navigateByUrl('/books');
          } else {
            this.messageService.addMessage('User ' + data.userName + ' was not authenticated');
          }
        },
        error => this.messageService.addMessage('Some error occured when logging in')
      );
  }

}
