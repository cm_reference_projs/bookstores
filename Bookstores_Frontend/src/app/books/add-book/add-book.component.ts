import {Component, OnInit} from "@angular/core";
import {Book} from "../book";
import {Author} from "../author";
import {BookService} from "../book.service";
import {Router} from "@angular/router";
import {MessageService} from "../../message/message.service";

@Component({
  selector: "app-add-book",
  templateUrl: "./add-book.component.html",
  styleUrls: ["./add-book.component.css"]
})
export class AddBookComponent implements OnInit {

  book: Book = new Book();
  author: Author = new Author();

  constructor(
    private bookService: BookService,
    private messageService: MessageService,
    private router: Router) {
  }

  ngOnInit() {
  }

  addNewBook() {
    this.book.Author = this.author;
    this.bookService.postBook(this.book).subscribe(
      () => {
        this.router.navigateByUrl('');
        this.clearForm();
      },
      () => this.messageService.addMessage('Cannot add book to the system')
    );
  }

  clearForm() {
    this.book = new Book();
    this.author = new Author();
  }
}
