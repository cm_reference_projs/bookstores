import { Component, OnInit } from '@angular/core';
import {Book} from "../book";
import {BookService} from "../book.service";
import {MessageService} from "../../message/message.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  book: Book = undefined;

  constructor(
    private bookService: BookService,
    private messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.bookService.getBook(id).subscribe(
      data => this.book = data,
      () => {
        this.messageService.addMessage('Cannot get details of book with id ' + id);
        this.router.navigate(['books']);
      }
    );
  }

  delete()
  {
    this.bookService.deleteBook(this.book.Id.toString()).subscribe(
      () => this.router.navigateByUrl(''),
      () => this.messageService.addMessage('Cannot delete book with id ' + this.book.Id)
    );
  }
}
