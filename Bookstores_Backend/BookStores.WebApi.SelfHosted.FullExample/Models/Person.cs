﻿using System;

namespace BookStores.WebApi.SelfHosted.FullExample.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDay { get; set; }
    }
}