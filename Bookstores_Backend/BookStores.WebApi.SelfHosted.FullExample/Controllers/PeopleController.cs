﻿using BookStores.WebApi.SelfHosted.FullExample.Models;
using BookStores.WebApi.SelfHosted.FullExample.Services;
using System.Collections.Generic;
using System.Web.Http;

namespace BookStores.WebApi.SelfHosted.Example.Controllers
{
    public class PeopleController : ApiController
    {
        private readonly IPeopleService _peopleService;

        public PeopleController(IPeopleService peopleService)
        {
            _peopleService = peopleService;
        }

        // GET api/people
        public IEnumerable<Person> Get()
        {
            return _peopleService.GetAll();
        }

        // GET api/people/5
        public Person Get(int id)
        {
            return _peopleService.GetById(id);
        }

        // POST api/people
        public void Post([FromBody]Person person)
        {
            _peopleService.Add(person);
        }

        // PUT api/people/5
        public void Put(int id, [FromBody]Person person)
        {
            _peopleService.Update(id, person);
        }

        // DELETE api/people/5
        public void Delete(int id)
        {
            _peopleService.Delete(id);
        }
    }
}