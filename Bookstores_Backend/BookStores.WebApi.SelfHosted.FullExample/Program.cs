﻿using BookStores.WebApi.SelfHosted.FullExample.Bootstrap;
using Microsoft.Owin.Hosting;
using System;
using BookStores.WebApi.SelfHosted.FullExample.Configuration;

namespace BookStores.WebApi.SelfHosted.FullExample
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var appConfig = new AppConfig();

            using (WebApp.Start<OwinBootstrapp>(appConfig.WepApiUri))
            {
                Console.ReadLine();
            }
        }
    }
}