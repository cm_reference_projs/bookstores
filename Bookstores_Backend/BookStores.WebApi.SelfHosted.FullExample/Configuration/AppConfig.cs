﻿using System.Configuration;

namespace BookStores.WebApi.SelfHosted.FullExample.Configuration
{
    internal class AppConfig
    {
        public string WepApiUri => ConfigurationManager.AppSettings["ThisWebApiUri"];
    }
}