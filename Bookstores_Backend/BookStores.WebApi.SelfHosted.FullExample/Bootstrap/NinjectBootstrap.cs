﻿using BookStores.WebApi.SelfHosted.FullExample.Configuration;
using BookStores.WebApi.SelfHosted.FullExample.Data;
using BookStores.WebApi.SelfHosted.FullExample.Services;
using Ninject;

namespace BookStores.WebApi.SelfHosted.FullExample.Bootstrap
{
    internal class NinjectBootstrap
    {
        public IKernel GetKernel()
        {
            var kernel = new StandardKernel();

            RegisterServices(kernel);
            RegisterRepositories(kernel);

            return kernel;
        }

        private void RegisterLogger(IKernel kernel)
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.AppSettings()
        }

        private void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IPeopleService>().To<PeopleService>();
        }

        private void RegisterRepositories(IKernel kernel)
        {
            kernel.Bind<IPeopleRepository>().To<PeopleRepository>();
        }
    }
}