﻿using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;
using System.Net.Http.Headers;
using System.Web.Http;

namespace BookStores.WebApi.SelfHosted.FullExample.Bootstrap
{
    internal class OwinBootstrapp
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host.
            HttpConfiguration config = new HttpConfiguration();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            appBuilder.UseNinjectMiddleware(new NinjectBootstrap().GetKernel).UseNinjectWebApi(config);
        }
    }
}