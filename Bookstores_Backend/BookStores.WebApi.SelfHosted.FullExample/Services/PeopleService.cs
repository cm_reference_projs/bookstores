﻿using BookStores.WebApi.SelfHosted.FullExample.Data;
using BookStores.WebApi.SelfHosted.FullExample.Models;
using System.Collections.Generic;

namespace BookStores.WebApi.SelfHosted.FullExample.Services
{
    public interface IPeopleService
    {
        IEnumerable<Person> GetAll();
        Person GetById(int id);
        void Add(Person person);
        void Update(int id, Person person);
        void Delete(int id);
    }

    internal class PeopleService : IPeopleService
    {
        private readonly IPeopleRepository _peopleRepository;

        public PeopleService(IPeopleRepository peopleRepository)
        {
            _peopleRepository = peopleRepository;
        }

        public IEnumerable<Person> GetAll()
        {
            return _peopleRepository.GetAll();
        }

        public Person GetById(int id)
        {
            return _peopleRepository.GetById(id);
        }

        public void Add(Person person)
        {
            _peopleRepository.Add(person);
        }

        public void Update(int id, Person person)
        {
            _peopleRepository.Update(id, person);
        }

        public void Delete(int id)
        {
            _peopleRepository.Delete(id);
        }
    }
}