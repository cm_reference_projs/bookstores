﻿using BookStores.WebApi.SelfHosted.FullExample.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BookStores.WebApi.SelfHosted.FullExample.Data
{
    public interface IPeopleRepository
    {
        IEnumerable<Person> GetAll();
        Person GetById(int id);
        void Add(Person person);
        void Update(int id, Person person);
        void Delete(int id);
    }

    internal class PeopleRepository : IPeopleRepository
    {
        private static List<Person> _peopleCollection = new List<Person>
        {
            new Person { Id = 1, Name = "Jan", Surname = "Nowak", BirthDay = new DateTime(1985, 10, 10) },
            new Person { Id = 2, Name = "Tomasz", Surname = "Kowalski", BirthDay = new DateTime(1995, 10, 10) },
            new Person { Id = 3, Name = "Piotr", Surname = "Kowalczyk", BirthDay = new DateTime(1955, 10, 10) },
            new Person { Id = 4, Name = "Katarzyna", Surname = "Plichta", BirthDay = new DateTime(1985, 10, 10) }
        };

        public IEnumerable<Person> GetAll()
        {
            return _peopleCollection;
        }

        public Person GetById(int id)
        {
            return _peopleCollection.SingleOrDefault(p => p.Id == id);
        }

        public void Add(Person person)
        {
            person.Id = _peopleCollection.Max(p => p.Id) + 1;
            _peopleCollection.Add(person);
        }

        public void Update(int id, Person person)
        {
            var personWithSameId = GetById(id);

            if(personWithSameId == null)
            {
                throw new ArgumentException($"There is no person with id {id}");
            }

            person.Id = id;
            var personIndex = _peopleCollection.IndexOf(personWithSameId);
            _peopleCollection[personIndex] = person;
        }

        public void Delete(int id)
        {
            _peopleCollection.Remove(GetById(id));
        }
    }
}