﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace BookStores.DataLayer.Models
{
    public class Book
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public Author Author { get; set; }
        public string Publisher { get; set; }
        public int YearOfPrint { get; set; }
        public List<BookStore> BookStores { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || obj as Book == null)
            {
                return false;
            }

            var objAsBook = obj as Book;

            var areEqual = true;
            areEqual = areEqual && objAsBook.Id == Id;
            areEqual = areEqual && objAsBook.Title == Title;
            areEqual = areEqual && objAsBook.YearOfPrint == YearOfPrint;
            areEqual = areEqual && objAsBook.Author.Equals(Author);

            areEqual = areEqual && objAsBook.BookStores.Count == BookStores.Count;
            foreach (var bookStore in BookStores)
            {
                areEqual = areEqual && objAsBook.BookStores.Any(bs => bs.Equals(bookStore));
            }

            return areEqual;
        }
    }
}