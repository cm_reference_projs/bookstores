﻿using System.Collections.Generic;

namespace BookStores.DataLayer.Models
{
    public class BookStore
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public List<Book> Books { get; set; }
    }
}
