﻿using System;

namespace BookStores.DataLayer.Models
{
    public class Author
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime YearOfBirth { get; set; }
    }
}
