﻿using System.Data.Entity;
using System.Linq;
using BookStores.DataLayer.DbContexts;
using BookStores.DataLayer.Models;

namespace BookStores.DataLayer.Repositories
{
    public class BookStoresRepository
    {
        public bool AddBookStore(BookStore bookStore)
        {
            var rowsAffected = 0;

            using (var dbContext = new BookStoresDbContext())
            {
                dbContext.BookStoresDbSet.Add(bookStore);
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected == 1;
        }

        public BookStore GetAllBookStoreDataById(int id)
        {
            BookStore bookStore = null;

            using (var dbContext = new BookStoresDbContext())
            {
                bookStore = dbContext.BookStoresDbSet
                    .Include(bs => bs.Books.Select(b => b.Author))
                    .Single(bs => bs.Id == id);
            }

            return bookStore;
        }
    }
}
