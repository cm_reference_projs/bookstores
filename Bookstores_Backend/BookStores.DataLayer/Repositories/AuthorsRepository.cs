﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BookStores.DataLayer.DbContexts;
using BookStores.DataLayer.Models;

namespace BookStores.DataLayer.Repositories
{
    public class AuthorsRepository
    {
        public Author GetAuthorById(int authorId)
        {
            Author author = null;

            using (var dbContext = new BookStoresDbContext())
            {
                author = dbContext.AuthorsDbSet.SingleOrDefault(a => a.Id == authorId);
            }

            return author;
        }

        public void UpdateAuthor(Author entity)
        {
            using (var dbContext = new BookStoresDbContext())
            {
                if (!dbContext.AuthorsDbSet.Local.Contains(entity))
                {
                    dbContext.AuthorsDbSet.Attach(entity);
                    dbContext.Entry(entity).State = EntityState.Modified;
                }

                dbContext.SaveChanges();
            }
        }

        public List<Author> GetAll()
        {
            using (var dbContext = new BookStoresDbContext())
            {
                return dbContext.AuthorsDbSet.ToList();
            }
        }
    }
}