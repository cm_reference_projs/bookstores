using System.Collections.Generic;
using BookStores.DataLayer.Models;

namespace BookStores.DataLayer.Repositories.Interfaces
{
    public interface IBooksRepository
    {
        List<Book> GetBooksByTitle(string title);
        bool AddBook(Book book);
        void UpdateBook(Book entity);
        Book GetBook(int id);
        void TransactionExample();
        IEnumerable<Book> GetAll();
        void Delete(int id);
    }
}