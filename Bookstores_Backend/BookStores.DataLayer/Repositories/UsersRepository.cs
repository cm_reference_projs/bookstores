﻿using System.Linq;
using BookStores.DataLayer.DbContexts;
using BookStores.DataLayer.Models;

namespace BookStores.DataLayer.Repositories
{
    public class UsersRepository
    {
        public void AddUser(User user)
        {
            using (var dbContext = new BookStoresDbContext())
            {
                dbContext.UsersDbSet.Add(user);
                dbContext.SaveChanges();
            }
        }

        public bool CheckUserPassword(User user)
        {
            using (var dbContext = new BookStoresDbContext())
            {
                var foundUser = dbContext.UsersDbSet.SingleOrDefault(u => u.UserName == user.UserName);

                if (foundUser == null)
                {
                    return false;
                }

                return foundUser.Password == user.Password;
            }
        }
    }
}