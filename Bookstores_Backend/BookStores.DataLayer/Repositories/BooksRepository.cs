﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using BookStores.DataLayer.DbContexts;
using BookStores.DataLayer.Models;
using BookStores.DataLayer.Repositories.Interfaces;
using System;

namespace BookStores.DataLayer.Repositories
{
    public class BooksRepository : IBooksRepository
    {
        public List<Book> GetBooksByTitle(string title)
        {
            List<Book> books = null;

            using (var dbContext = new BookStoresDbContext())
            {
                books = dbContext.BooksDbSet.Where(b => b.Title == title).ToList();
            }

            return books;
        }

        public bool AddBook(Book book)
        {
            Book addedBook = null;

            using (var dbContext = new BookStoresDbContext())
            {
                addedBook = dbContext.BooksDbSet.Add(book);
                dbContext.SaveChanges();
            }

            return addedBook != null;
        }

        public void UpdateBook(Book entity)
        {
            using (var dbContext = new BookStoresDbContext())
            {
                if (!dbContext.AuthorsDbSet.Local.Contains(entity.Author))
                {
                    dbContext.AuthorsDbSet.Attach(entity.Author);
                    dbContext.Entry(entity.Author).State = EntityState.Modified;
                }

                if (!dbContext.BooksDbSet.Local.Contains(entity))
                {
                    dbContext.BooksDbSet.Attach(entity);
                    dbContext.Entry(entity).State = EntityState.Modified;
                }

                dbContext.SaveChanges();
            }
        }

        public Book GetBook(int id)
        {
            Book book = null;

            using (var dbContext = new BookStoresDbContext())
            {
                book = dbContext.BooksDbSet
                    .Include(b => b.Author)
                    .Single(b => b.Id == id);
            }

            return book;
        }

        public void TransactionExample()
        {
            using (var dbContext = new BookStoresDbContext())
            using (var transaction = dbContext.Database.BeginTransaction())
            {
                try
                {
                    Author author = new Author
                    {
                        Name = "Jan",
                        Surname = "Nowak",
                        YearOfBirth = new DateTime(1925, 06, 10)
                    };
                    dbContext.AuthorsDbSet.Add(author);
                    dbContext.SaveChanges();

                    throw new Exception("Custom Exception");

                    Book book = new Book
                    {
                        Title = "Some Book",
                        Author = author
                    };
                    dbContext.BooksDbSet.Add(book);
                    dbContext.SaveChanges();

                    transaction.Commit();
                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                    throw new Exception("Some error occured. Rollbacking transaction", exception);
                }
            }
        }

        public void DeleteBook(int id)
        {
            using (var dbContext = new BookStoresDbContext())
            {
                var book = dbContext.BooksDbSet
                    .Include(b => b.Author)
                    .Single(b => b.Id == id);
                dbContext.BooksDbSet.Remove(book);
                dbContext.SaveChanges();
            }

        }

        public IEnumerable<Book> GetAll()
        {
            using (var dbContext = new BookStoresDbContext())
            {
                return dbContext.BooksDbSet.ToList();
            }
        }

        public void Delete(int id)
        {
            using (var dbContext = new BookStoresDbContext())
            {
                var bookToDelete = dbContext.BooksDbSet.SingleOrDefault(b => b.Id == id);
                dbContext.BooksDbSet.Remove(bookToDelete);
                dbContext.SaveChanges();
            }
        }
    }
}