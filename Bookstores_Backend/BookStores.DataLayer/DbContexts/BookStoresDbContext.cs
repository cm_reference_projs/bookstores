﻿using System.Configuration;
using System.Data.Entity;
using BookStores.DataLayer.Models;

namespace BookStores.DataLayer.DbContexts
{
    internal class BookStoresDbContext : DbContext
    {
        public BookStoresDbContext() : base(GetConnectionString())
        { }

        public DbSet<Author> AuthorsDbSet { get; set; }
        public DbSet<Book> BooksDbSet { get; set; }
        public DbSet<BookStore> BookStoresDbSet { get; set; }
        public DbSet<User> UsersDbSet { get; set; }

        private static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["BookStoresSqlDb"].ConnectionString;
        }
    }
}