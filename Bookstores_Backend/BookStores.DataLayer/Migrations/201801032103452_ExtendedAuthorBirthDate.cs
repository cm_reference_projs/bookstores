namespace BookStores.DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExtendedAuthorBirthDate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Authors", "YearOfBirth", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Authors", "YearOfBirth", c => c.Int(nullable: false));
        }
    }
}
