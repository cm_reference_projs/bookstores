namespace BookStores.DataLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initials : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Authors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                        YearOfBirth = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Books",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        YearOfPrint = c.Int(nullable: false),
                        Author_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Authors", t => t.Author_Id)
                .Index(t => t.Author_Id);
            
            CreateTable(
                "dbo.BookStores",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        City = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BookStoreBooks",
                c => new
                    {
                        BookStore_Id = c.Int(nullable: false),
                        Book_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.BookStore_Id, t.Book_Id })
                .ForeignKey("dbo.BookStores", t => t.BookStore_Id, cascadeDelete: true)
                .ForeignKey("dbo.Books", t => t.Book_Id, cascadeDelete: true)
                .Index(t => t.BookStore_Id)
                .Index(t => t.Book_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BookStoreBooks", "Book_Id", "dbo.Books");
            DropForeignKey("dbo.BookStoreBooks", "BookStore_Id", "dbo.BookStores");
            DropForeignKey("dbo.Books", "Author_Id", "dbo.Authors");
            DropIndex("dbo.BookStoreBooks", new[] { "Book_Id" });
            DropIndex("dbo.BookStoreBooks", new[] { "BookStore_Id" });
            DropIndex("dbo.Books", new[] { "Author_Id" });
            DropTable("dbo.BookStoreBooks");
            DropTable("dbo.BookStores");
            DropTable("dbo.Books");
            DropTable("dbo.Authors");
        }
    }
}
