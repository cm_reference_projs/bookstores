﻿using BookStores.WebApi.SelfHosted.Example.OwinSelfhostSample;
using Microsoft.Owin.Hosting;
using System;
using System.Net.Http;

namespace BookStores.WebApi.SelfHosted.Example
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string baseAddress = "http://localhost:9000/";

            // Start OWIN host
            using (WebApp.Start<StartUp>(url: baseAddress))
            {
                // Create HttpCient and make a request to api/values
                HttpClient client = new HttpClient();

                var response = client.GetAsync(baseAddress + "api/example").Result;

                Console.WriteLine(response);
                Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                Console.ReadLine();
            }
        }
    }
}
