﻿using System.Collections.Generic;
using System.Web.Http;

namespace BookStores.WebApi.SelfHosted.Example.Controllers
{
    public class ExampleController : ApiController
    {
        // GET api/example
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/example/5
        public string Get([FromUri] int id)
        {
            return "value";
        }

        // POST api/example
        public void Post([FromBody] string value)
        {
        }

        // PUT api/example/5
        public void Put([FromUri] int id, [FromBody] string value)
        {
        }

        // DELETE api/example/5
        public void Delete([FromUri] int id)
        {
        }
    }
}