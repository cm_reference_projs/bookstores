﻿using System;
using BookStores.BusinessLayer.Dtos;
using BookStores.BusinessLayer.Services;
using BookStores.Cli.IoHelpers;

namespace BookStores.Cli
{
    internal class ProgramLoop
    {
        public enum CommandTypes { AddBook, GetAuthorById, CheckBookByTitle, CreateTestBookStores, PrintBookStoreById, TransactionExample, EditBook, Exit }

        public void Execute()
        {
            var exit = false;
            while (!exit)
            {
                var command = ConsoleReadHelper.GetCommnadType("Provide a command: ");

                switch (command)
                {
                    case CommandTypes.CreateTestBookStores:
                        CreateTestBookStores();
                        break;
                    case CommandTypes.PrintBookStoreById:
                        PrintBookStoresData();
                        break;
                    case CommandTypes.CheckBookByTitle:
                        CheckBookByTitle();
                        break;
                    case CommandTypes.AddBook:
                        AddBook();
                        break;
                    case CommandTypes.EditBook:
                        EditBook();
                        break;
                    case CommandTypes.GetAuthorById:
                        GetAuthorById();
                        break;
                    case CommandTypes.TransactionExample:
                        new BooksService().TransactionExample();
                        break;
                    case CommandTypes.Exit:
                        exit = true;
                        break;
                    default:
                        Console.WriteLine("Command " + command + " is not supported");
                        break;
                }
            }
        }

        private void GetAuthorById()
        {
            var authorService = new AuthorsService();
            var author = authorService.GetAuthorById(ConsoleReadHelper.GetInt("Provide author Id: "));

            if (author == null)
            {
                Console.WriteLine("There is no author with such Id");
            }
            else
            {
                Console.WriteLine(author.Name + " " + author.Surname);
            }
        }

        private void EditBook()
        {
            var booksService = new BooksService();

            var book = booksService.GetBook(ConsoleReadHelper.GetInt("Podaj id ksiazki: "));

            book.Title = ConsoleReadHelper.GetString("New title [" + book.Title + "]: ");
            book.Author.Name = ConsoleReadHelper.GetString("New author name [" + book.Author.Name + "]: ");

            booksService.UpdateBook(book);
        }

        private void AddBook()
        {
            Console.WriteLine("Please provide book data: ");

            var book = new BookDto();
            Console.Write("Book title: ");
            book.Title = Console.ReadLine();
            Console.Write("Book year of print: ");
            book.YearOfPrint = int.Parse(Console.ReadLine());

            var success = false;

            try
            {
                var booksService = new BooksService();
                success = booksService.AddBook(book);
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occured when adding a book: " + e.Message);
            }

            ConsoleWriteHelper.PrintOperationSuccessMessage(success);
        }

        private void CheckBookByTitle()
        {
            Console.Write("Give me a title to check if it exists in the system: ");
            var titleToCheck = Console.ReadLine();

            var booksService = new BooksService();

            if (booksService.CheckIfBookExistsInTheSystemByTitle(titleToCheck))
            {
                Console.WriteLine("Book exists");
            }
            else
            {
                Console.WriteLine("Book does not exist");
            }
        }

        private void CreateTestBookStores()
        {
            var bookStoresService = new BookStoresService();
            var success = bookStoresService.CreateTestBookStores();

            ConsoleWriteHelper.PrintOperationSuccessMessage(success);
        }

        private void PrintBookStoresData()
        {
            var bookStoreId = ConsoleReadHelper.GetInt("Provide BookStore id: ");

            var bookStoresService = new BookStoresService();
            var bookStore = bookStoresService.GetBookStoreData(bookStoreId);

            ConsoleWriteHelper.PrintBookStoreDto(bookStore);
        }
    }
}
