﻿using System;

namespace BookStores.Cli.IoHelpers
{
    internal class ConsoleReadHelper
    {
        public static int GetInt(string message)
        {
            int number;
            Console.Write(message);

            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Not an integer number - try again...");
            }

            return number;
        }

        public static ProgramLoop.CommandTypes GetCommnadType(string message)
        {
            ProgramLoop.CommandTypes commandType;
            Console.Write(message + " (AddBook, EditBook, CheckBookByTitle, CreateTestBookStores, PrintBookStoreById, Exit) ");

            while (!Enum.TryParse(Console.ReadLine(), out commandType))
            {
                Console.WriteLine("Not a proper command type - try again...");
            }

            return commandType;
        }

        public static string GetString(string message)
        {
            Console.Write(message);
            return Console.ReadLine();
        }
    }
}
