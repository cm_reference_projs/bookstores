﻿using System;
using System.Linq;
using BookStores.BusinessLayer.Dtos;

namespace BookStores.Cli.IoHelpers
{
    internal class ConsoleWriteHelper
    {
        public static void PrintBookStoreDto(BookStoreDto bookStore)
        {
            Console.WriteLine(bookStore.Name + " " + bookStore.City + " (contains kids books: " + bookStore.HaveKidsBooks);
            Console.WriteLine("Available books:");
            bookStore.AvailableBooks.ForEach(b => PrintBookDto(b));
        }

        public static void PrintBookDto(BookDto book)
        {
            Console.Write(book.Title + " (" + book.YearOfPrint + ") by ");

            if(book.Author.IsModentAuthor)
                Console.Write("modern author ");

            PrintAuthorDto(book.Author);
        }

        public static void PrintAuthorDto(AuthorDto author)
        {
            Console.WriteLine(author.Name + " "  + author.Surname);
        }

        public static void PrintOperationSuccessMessage(bool success)
        {
            if (success)
            {
                Console.WriteLine("Operation succeeded");
            }
            else
            {
                Console.WriteLine("Operation failed");
            }
        }
    }
}
