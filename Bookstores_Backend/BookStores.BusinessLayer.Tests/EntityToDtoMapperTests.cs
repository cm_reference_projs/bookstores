﻿using System;
using BookStores.BusinessLayer.Dtos;
using BookStores.BusinessLayer.Mappers;
using BookStores.DataLayer.Models;
using NUnit.Framework;

namespace BookStores.BusinessLayer.Tests
{
    [TestFixture]
    public class EntityToDtoMapperTests
    {
        [Test]
        public void BookMapping_ProvideValidBook_ReceiveProperlyMappedBookDto()
        {
            var bookToMap = new Book
            {
                Id = 1,
                Title = "SomeBook",
                YearOfPrint = 2000,
                Author = new Author
                {
                    Id = 1,
                    Name = "John",
                    Surname = "Smith",
                    YearOfBirth = new DateTime(1990, 12, 25)
                }
            };

            var expectedDto = new BookDto
            {
                Id = 1,
                Title = "SomeBook",
                YearOfPrint = 2000,
                Author = new AuthorDto
                {
                    Id = 1,
                    Name = "John",
                    Surname = "Smith",
                    IsModentAuthor = new DateTime(1990, 10, 10).Year > DateTime.Now.Year - 35
                }
            };

            var resultOfMapping = EntityToDtoMapper.BookEntityModelToDto(bookToMap);

            Assert.AreEqual(expectedDto.Id, resultOfMapping.Id);
        }
    }
}