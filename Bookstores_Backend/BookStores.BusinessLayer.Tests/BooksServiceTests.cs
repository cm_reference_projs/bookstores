﻿using System.Collections.Generic;
using BookStores.BusinessLayer.Services;
using BookStores.DataLayer.Models;
using BookStores.DataLayer.Repositories.Interfaces;
using Moq;
using NUnit.Framework;

namespace BookStores.BusinessLayer.Tests
{
    [TestFixture]
    public class BookServiceTests
    {
        [Test]
        public void WhenCheckingBookExistacne_GiveValidBookTitle_ReturnTrue()
        {
            //Arrange
            var bookList = new List<Book>(); // konstruktor
            bookList.Add(new Book()); // dodajemy ksiazke obojetnie jaka do listy
            //tworzymy mocka
            var booksRepositoryMock = new Mock<IBooksRepository>();
            booksRepositoryMock.Setup(x => x.GetBooksByTitle(It.IsAny<string>())).Returns(bookList); //cookbook zamiast string moze byc

            var booksService = new BooksService(booksRepositoryMock.Object); //interfacami - podajemy mocka w parametrze
            //nazwe ksiazki podamy jako parametr

            //Act

            var result = booksService.CheckIfBookExistsInTheSystemByTitle("CookBook");

            //Assert
            Assert.IsTrue(result);
            booksRepositoryMock.Verify(x => x.GetBooksByTitle("CookBook"), Times.Once); //czy na pewno zapytal ta funkcje z odpowiednim parametrem
            //kazdy test sprawdza jednen przypadek

        }

        [Test]
        public void WhenCheckingBookExistacne_NotExisting_ReturnFalse()
        {
            //Arrange
            List<Book> bookList = null; // konstruktor

            //tworzymy mocka
            var booksRepositoryMock = new Mock<IBooksRepository>();
            booksRepositoryMock.Setup(x => x.GetBooksByTitle(It.IsAny<string>())).Returns(bookList); //cookbook zamiast string moze byc
            var booksService = new BooksService(booksRepositoryMock.Object); //interfacami - podajemy mocka w parametrze
            //nazwe ksiazki podamy jako parametr

            //Act

            var result = booksService.CheckIfBookExistsInTheSystemByTitle("");

            //Assert
            Assert.IsFalse(result);
            booksRepositoryMock.Verify(x => x.GetBooksByTitle(""), Times.Once);
            //kazdy test sprawdza jednen przypadek

        }

        //TAK SIE ROBI TESTY z uzyciem (moq) ddoatkiem
        [Test]
        public void WhenCheckingBookExistacne_ListIsEqualZero_ReturnFalse()
        {
            //Arrange
            //tworzymy mocka
            var booksRepositoryMock = new Mock<IBooksRepository>(); //posiada metody, ale nie sa zaimplementowane
            //setupv (wybieram metode ktora jest dostepna w interfejsie)
            //dla obojetnie jakiego stringa zwroc nam pusta liste
            //ustawiamy wartosci obiektu
            booksRepositoryMock.Setup(x => x.GetBooksByTitle(It.IsAny<string>())).Returns(new List<Book>()); //zwroc na pusta ;liste

            //wywolujemy ten obiekt tutaj
            var booksService = new BooksService(booksRepositoryMock.Object);


            //Act
            var result = booksService.CheckIfBookExistsInTheSystemByTitle("dasd");

            //Assert
            Assert.IsFalse(result);
            booksRepositoryMock.Verify(x => x.GetBooksByTitle("dasd"), Times.Once);

            //kazdy test sprawdza jednen przypadek

        }

    }
}