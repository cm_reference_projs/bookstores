﻿using System;
using System.Linq;
using BookStores.BusinessLayer.Dtos;
using BookStores.DataLayer.Models;

namespace BookStores.BusinessLayer.Mappers
{
    public class EntityToDtoMapper
    {
        //w przypadki bardziej rozbudowanej logiki biznesowej naturalnie logike zamykamy w metody albo nawet wydzielamy od osobnych klas
        //Dodatkowo, logika nie musi miec miejsca tu, gdzie dokonujemy mapowania. Moze sie odbywac przed lub po w serwisie, ktory mapowania dokonuje

        public static BookStoreDto BookStoreEntityModelToDto(BookStore bookStore)
        {
            if (bookStore == null)
            {
                return null;
            }

            var bookStoreDto = new BookStoreDto();

            bookStoreDto.Id = bookStore.Id;
            bookStoreDto.Name = bookStore.Name;
            bookStoreDto.City = bookStore.City;

            bookStoreDto.AvailableBooks = bookStore.Books == null ? null : bookStore.Books.Select(b => BookEntityModelToDto(b)).ToList();
            bookStoreDto.HaveKidsBooks = bookStore.Books == null ? false : bookStore.Books.Count(b => b.Title.ToLower().Contains("fairytale")) > 0; //bardzo prosta logika biznesowa

            return bookStoreDto;
        }

        public static BookDto BookEntityModelToDto(Book book)
        {
            if (book == null)
            {
                return null;
            }

            var bookDto = new BookDto();

            bookDto.Id = book.Id;
            bookDto.Title = book.Title;
            bookDto.Author = AuthorEntityModelToDto(book.Author);
            bookDto.YearOfPrint = book.YearOfPrint;

            return bookDto;
        }

        public static AuthorDto AuthorEntityModelToDto(Author author)
        {
            if (author == null)
            {
                return null;
            }

            var authorDto = new AuthorDto();

            authorDto.Id = author.Id;
            authorDto.Name = author.Name;
            authorDto.Surname = author.Surname;
            authorDto.YearOfBirth = author.YearOfBirth;
            authorDto.IsModentAuthor = author.YearOfBirth.Year > DateTime.Now.Year - 35; //bardzo prosta logika biznesowa

            return authorDto;
        }

        public static UserDto UserEntityModelToDto(User user)
        {
            if (user == null)
            {
                return null;
            }

            var userDto = new UserDto
            {
                Id = user.Id,
                UserName = user.UserName,
                Password = user.Password
            };

            return userDto;
        }
    }
}