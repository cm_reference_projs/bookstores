﻿using BookStores.BusinessLayer.Dtos;
using BookStores.DataLayer.Models;

namespace BookStores.BusinessLayer.Mappers
{
    internal class DtoToEntityMapper
    {
        public static Book BookDtoToEntityModel(BookDto bookDto)
        {
            if (bookDto == null)
            {
                return null;
            }

            var book = new Book();

            book.Id = bookDto.Id;
            book.Title = bookDto.Title;
            book.Author = AuthorEntityModelToDto(bookDto.Author);
            book.YearOfPrint = bookDto.YearOfPrint;

            return book;
        }

        public static Author AuthorEntityModelToDto(AuthorDto authorDto)
        {
            if (authorDto == null)
            {
                return null;
            }

            var author = new Author();

            author.Id = authorDto.Id;
            author.Name = authorDto.Name;
            author.Surname = authorDto.Surname;
            author.YearOfBirth = authorDto.YearOfBirth;

            return author;
        }

        public static User UserEntityModelToDto(UserDto userDto)
        {
            if (userDto == null)
            {
                return null;
            }

            var user = new User
            {
                Id = userDto.Id,
                UserName = userDto.UserName,
                Password = userDto.Password
            };

            return user;
        }
    }
}