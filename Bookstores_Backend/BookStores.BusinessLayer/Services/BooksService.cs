﻿using System;
using BookStores.BusinessLayer.Dtos;
using BookStores.BusinessLayer.Mappers;
using BookStores.DataLayer.Repositories;
using BookStores.DataLayer.Repositories.Interfaces;
using System.Collections;
using System.Collections.Generic;

namespace BookStores.BusinessLayer.Services
{
    public class BooksService
    {
        private IBooksRepository _booksRepository;

        public BooksService()
        {
            _booksRepository = new BooksRepository();
        }

        public BooksService(IBooksRepository booksRepository)
        {
            _booksRepository = booksRepository;
        }

        public bool CheckIfBookExistsInTheSystemByTitle(string title)
        {
            var books = _booksRepository.GetBooksByTitle(title);

            if (books == null || books.Count == 0)
            {
                return false;
            }

            return true;
        }

        public bool AddBook(BookDto bookDto)
        {
            if (CheckIfBookExistsInTheSystemByTitle(bookDto.Title))
            {
                throw new Exception("Book with the same title exists!");
            }

            var book = DtoToEntityMapper.BookDtoToEntityModel(bookDto);

            return _booksRepository.AddBook(book);
        }

        public void DeleteBook(int id)
        {
            _booksRepository.Delete(id);
        }

        public BookDto GetBook(int id)
        {
            var book = _booksRepository.GetBook(id);
            return EntityToDtoMapper.BookEntityModelToDto(book);
        }

        public void UpdateBook(BookDto bookDto)
        {
            var book = DtoToEntityMapper.BookDtoToEntityModel(bookDto);
            _booksRepository.UpdateBook(book);
        }

        public void TransactionExample()
        {
            _booksRepository.TransactionExample();
        }

        public IEnumerable<BookDto> GetAll()
        {
            var bookDtos = new List<BookDto>();

            foreach (var book in _booksRepository.GetAll())
            {
                var bookDto = EntityToDtoMapper.BookEntityModelToDto(book);
                bookDtos.Add(bookDto);
            }

            return bookDtos;
        }
    }
}