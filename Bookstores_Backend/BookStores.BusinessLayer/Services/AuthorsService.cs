﻿using System.Collections.Generic;
using BookStores.BusinessLayer.Dtos;
using BookStores.BusinessLayer.Mappers;
using BookStores.DataLayer.Repositories;

namespace BookStores.BusinessLayer.Services
{
    public class AuthorsService
    {
        private readonly AuthorsRepository _authorsRepository;

        public AuthorsService()
        {
            _authorsRepository = new AuthorsRepository();
        }

        public List<AuthorDto> GetAllAuthors()
        {
            var authorDtos = new List<AuthorDto>();

            foreach (var author in _authorsRepository.GetAll())
            {
                var authorDto = EntityToDtoMapper.AuthorEntityModelToDto(author);
                authorDtos.Add(authorDto);
            }

            return authorDtos;
        }

        public AuthorDto GetAuthorById(int authorId)
        {
            var author = _authorsRepository.GetAuthorById(authorId);
            return EntityToDtoMapper.AuthorEntityModelToDto(author);
        }

        public void UpdateAuthor(AuthorDto authorDto)
        {
            var author = DtoToEntityMapper.AuthorEntityModelToDto(authorDto);
            _authorsRepository.UpdateAuthor(author);
        }
    }
}