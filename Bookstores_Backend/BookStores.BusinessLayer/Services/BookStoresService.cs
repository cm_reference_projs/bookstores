﻿using System;
using System.Collections.Generic;
using BookStores.BusinessLayer.Dtos;
using BookStores.BusinessLayer.Mappers;
using BookStores.DataLayer.Models;
using BookStores.DataLayer.Repositories;

namespace BookStores.BusinessLayer.Services
{
    public class BookStoresService
    {
        public bool CreateTestBookStores()
        {
            var john = new Author { Name = "John", Surname = "Smith", YearOfBirth = new DateTime(1980, 01, 01) };
            var jackie = new Author { Name = "Jackie", Surname = "Chang", YearOfBirth = new DateTime(1965, 05, 05) };
            var kate = new Author { Name = "Kate", Surname = "Wozniak", YearOfBirth = new DateTime(1990, 10, 30) };

            var cookBook = new Book { Title = "Cook book", Author = john, YearOfPrint = 2000 };
            var travelBook = new Book {Title = "Beautiful fairytales", Author = jackie, YearOfPrint = 1990 };
            var programmingBook = new Book {Title = "Programming book", Author = kate, YearOfPrint = 2010 };

            var empik = new BookStore { Name = "Empik", City = "Gdansk", Books = new List<Book> { cookBook, travelBook } };
            var domKsiazki = new BookStore { Name = "Dom Ksiazki", City = "Gdynia", Books = new List<Book> { cookBook, programmingBook } };

            var success = true;
            var bookStoreRepository = new BookStoresRepository();
            success &= bookStoreRepository.AddBookStore(empik);
            success &= bookStoreRepository.AddBookStore(domKsiazki);

            var testUser = new User
            {
                UserName = "TestUser",
                Password = "123456"
            };

            var usersRepository = new UsersRepository();

            if (!usersRepository.CheckUserPassword(testUser))
            {
                usersRepository.AddUser(testUser);
            }

            return success;
        }

        public BookStoreDto GetBookStoreData(int id)
        {
            var bookStoreRepository = new BookStoresRepository();
            var bookStore = bookStoreRepository.GetAllBookStoreDataById(id);

            return EntityToDtoMapper.BookStoreEntityModelToDto(bookStore);
        }
    }
}