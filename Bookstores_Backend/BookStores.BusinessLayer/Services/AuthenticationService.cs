﻿using BookStores.BusinessLayer.Dtos;
using BookStores.BusinessLayer.Mappers;
using BookStores.DataLayer.Repositories;

namespace BookStores.BusinessLayer.Services
{
    public class AuthenticationService
    {
        private readonly UsersRepository _usersRepository;

        public AuthenticationService()
        {
            _usersRepository =  new UsersRepository();
        }

        public AuthenticationResultDto CheckUserPassword(UserDto userDto)
        {
            var user = DtoToEntityMapper.UserEntityModelToDto(userDto);

            return new AuthenticationResultDto
            {
                UserName = userDto.UserName,
                IsAutheticated = _usersRepository.CheckUserPassword(user)
            };
        }
    }
}