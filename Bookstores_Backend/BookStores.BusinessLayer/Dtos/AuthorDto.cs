﻿using System;

namespace BookStores.BusinessLayer.Dtos
{
    public class AuthorDto
    {
        public int Id;
        public string Name;
        public string Surname;
        public DateTime YearOfBirth;
        public bool IsModentAuthor;
    }
}