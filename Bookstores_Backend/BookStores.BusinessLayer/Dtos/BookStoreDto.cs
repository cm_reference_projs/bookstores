﻿using System.Collections.Generic;

namespace BookStores.BusinessLayer.Dtos
{
    public class BookStoreDto
    {
        public int Id;
        public string Name;
        public string City;
        public List<BookDto> AvailableBooks;
        public bool HaveKidsBooks;
    }
}
