﻿namespace BookStores.BusinessLayer.Dtos
{
    public class AuthenticationResultDto
    {
        public string UserName { get; set; }
        public bool IsAutheticated { get; set; }
    }
}