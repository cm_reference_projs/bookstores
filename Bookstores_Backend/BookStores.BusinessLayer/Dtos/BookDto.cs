﻿namespace BookStores.BusinessLayer.Dtos
{
    public class BookDto
    {
        public int Id;
        public string Title;
        public AuthorDto Author;
        public int YearOfPrint;
    }
}