﻿using BookStores.BusinessLayer.Services;
using System.Web.Http;

namespace BookStores.WebApi.Controllers
{
    public class TestDataController : ApiController
    {
        private readonly BookStoresService _bookStoresService = new BookStoresService();

        public void Get()
        {
            _bookStoresService.CreateTestBookStores();
        }
    }
}