﻿using System.Web.Http;
using BookStores.BusinessLayer.Dtos;
using BookStores.BusinessLayer.Services;

namespace BookStores.WebApi.Controllers
{
    public class AuthenticationController : ApiController
    {
        private readonly AuthenticationService _authenticationService;

        public AuthenticationController()
        {
            _authenticationService = new AuthenticationService();
        }

        [HttpPost]
        public AuthenticationResultDto CheckUser(UserDto user)
        {
            return _authenticationService.CheckUserPassword(user);
        }
    }
}