﻿using BookStores.BusinessLayer.Dtos;
using BookStores.BusinessLayer.Services;
using System.Collections.Generic;
using System.Web.Http;

namespace BookStores.WebApi.Controllers
{
    public class BooksController : ApiController
    {
        private readonly BooksService _booksService = new BooksService();

        [HttpGet]
        public BookDto DownloadBook(int id)
        {
            return _booksService.GetBook(id);
        }

        [HttpPost]
        public void AddBook(BookDto bookDto)
        {
            _booksService.AddBook(bookDto);
        }

        [HttpPut]
        public void Put([FromUri] int id, [FromBody] BookDto book)
        {
            book.Id = id;
            _booksService.UpdateBook(book);
        }

        [HttpDelete]
        public void DeleteBook(int id)
        {
            _booksService.DeleteBook(id);
        }

        [HttpGet]
        public IEnumerable<BookDto> GetAllBooks()
        {
            return _booksService.GetAll();
        }
    }
}