﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using BookStores.BusinessLayer.Dtos;
using BookStores.BusinessLayer.Services;

namespace BookStores.WebApi.Controllers
{
    public class AuthorsController : ApiController
    {
        private readonly AuthorsService _authorsService = new AuthorsService();

        [HttpGet]
        public AuthorDto GetAuthorById(int id)
        {
            return _authorsService.GetAuthorById(id);
        }

        [HttpPut]
        public void Put([FromUri] int id, [FromBody] AuthorDto author)
        {
            author.Id = id;
            _authorsService.UpdateAuthor(author);
        }

        [HttpGet]
        public IEnumerable<AuthorDto> GetAllAuthors()
        {
            return _authorsService.GetAllAuthors();
        }
    }
}