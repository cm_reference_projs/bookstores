﻿using System.Web.Http;

namespace BookStores.WebApi.Controllers
{
    public class StatusController : ApiController
    {
        public string Get()
        {
            return "OK";
        }
    }
}